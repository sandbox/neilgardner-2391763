<?php

/*
 * Build HTML for a set of field items, called from doublefield_formatter_field_formatter_view
* @param array $items
* @param array $display
* @return array
*/
function doublefield_formatter_build_element($items,$display) {
	$settings = (isset($display['settings']) && is_array($display['settings']))? $display['settings'] : array();
	$element = array();
	
	if (!empty($items) && is_array($items)) {
		foreach ($items as $delta => $item) {
			$lastIndex = count($items) - 1;
			if (isset($item['first']) && isset($item['second'])) {
				$hasFirst = !empty($item['first']);
				$hasSecond = !empty($item['second']);
				if ($hasFirst || $hasSecond) {
					$html = '';
					$suppressIfEmpty = (isset($settings['suppress_if_empty']) && $settings['suppress_if_empty'] == 1);
					$numSubFields = 0;
					if (!$suppressIfEmpty || $hasFirst) {
						$html .= _doublefield_formatter_build_tag_item($settings, $item['first'],'first');
						$numSubFields++;
					}
					if (!$suppressIfEmpty || $hasSecond) {
						$html .= _doublefield_formatter_build_tag_item($settings,$item['second'],'second');
						$numSubFields++;
					}
					if (!empty($settings['wrapper_tag']) && is_string($settings['wrapper_tag'])) {
						if (!isset($settings['wrapper_classes']) || !is_string($settings['wrapper_classes'])) {
							$settings['wrapper_classes'] = '';
						} else {
							$settings['wrapper_classes'] = trim($settings['wrapper_classes']);
						}
						if ($suppressIfEmpty) {
							$extraClass = $numSubFields > 1? 'double-item' : 'single-item';
							$settings['wrapper_classes'] .= ' ' . $extraClass;
						}
						$output = _doublefield_formatter_build_tag($settings['wrapper_tag'],$html, $settings['wrapper_classes']);
					} else {
						$output = $html;
					}
					$element[$delta] = array('#markup' =>  $output);
				}
			}
		}
	}
	
	
	return $element;
}

/*
 * Build a tag for a specific item from $settings array
* @param array &$settings
* @param string $content
* @param string $type
*/
function _doublefield_formatter_build_tag_item(&$settings,$content,$type = 'first') {
	if (is_string($content)) {
		if (!isset($settings[$type]['format'])) {
			$settings[$type]['format'] = '_none';
		}
		switch ($settings[$type]['format']) {
			case '_none':
				$content = check_plain($content);
				break;
			default:
				if (preg_match('#<\w+[^>]*?>#', $content)) {
					$content = check_markup($content, $settings[$type]['format']);
				}
				break;
		}
	}
	return _doublefield_formatter_build_tag($settings[$type]['tag'], $settings[$type]['prefix'] . $content . $settings[$type]['suffix'], $settings[$type]['classes']);
}

/*
 * Build classes from an array or string
* @param moxed $class_names
* @return array
*/
function _doublefield_formatter_build_classes($class_names = NULL) {
	$classes = array();
	if (!empty($class_names)) {
		if (is_string($class_names)) {
			$classes = explode(' ',trim($class_names));
		}
	} else if (is_array($class_names)) {
		$classes = $class_names;
	}
	return $classes;
}

/*
 * Build a tag from simple paramters 
 * @param string $tagName
 * @param string $content
 * @param array $class_names
 * @return string
 */
function _doublefield_formatter_build_tag($tagName = NULL, $content = NULL, $class_names = array()) {
	$html = '';
	$classes = _doublefield_formatter_build_classes($class_names);
	$tagName = trim($tagName);
	if (!empty($tagName)) {
		if (!empty($classes) && is_array($classes)) {
			$strAttrs = ' class="'.implode(' ', $classes).'"';
		}
		$html .= '<'.$tagName.$strAttrs . '>';
	}
	if (is_string($content)) {
		$html .= $content;
	}
	if (!empty($tagName)) {
		$html .= '</'.$tagName.'>';
	}
	return $html;
}