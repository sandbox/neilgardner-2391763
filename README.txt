Date Components provides an alternative formatter for the date field module. 
Each date and time component is individually wrapped in an HTML tag with semantic class names to facilitate theming.
You may wish to style months differently from days and years, to add extra spacing between date components or position elements vertically.

Settings:
When you first select "date components" as a formatter for your date field, the default settings are based on your site's long date format.
Drupal expects users to configure a set of custom date formats (Admin > Configuration > Regional > Date Formats) with three different formats installed by default: long, medium and short. If you set your locale correctly (e.g. en-GB or es-MX) during installation, Drupal should offer date formats consistent with conventions in your country, e.g. February 23, 2014 for the US, but 23 February 2014 for the UK.
However, these formats do not allow you to easily insert HTML tags (without escaping every single letter in a tag or attribute) or add other semantic atrributes. The default formats also assume you wish to display times although you could fix this by defining your own custom formats. 
We thus allow you to configure most common date format options:
1) Time format: We assume either zero-padded 24 hour or 12 twelve with am/pm suffixes
2) Day name format: We assume short or long day name formats, see notes on localisation
3) Month format: Numeric, short English, long English or custom , see notes on localisation
4) Year format: 2-digit or 4 digit
5) Date order: Should be based on your locale settings, but please check.

Theming:
A tenplate would be very complicated as most attributes are contextual and we only use two tag variants. Thus the renderer class (DateComponentsBuilder) has a magic method BuildTag that translates dynamic