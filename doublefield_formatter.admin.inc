<?php
/*
 * Admin functions called via field formatter hooks 
*/

/*
 * Private function called by in doublefield_formatter_field_formatter_settings_form in module fule
 * @param array $field
 * @param array $display
 * @param array $form
 * @param array $form_state
 * @return array
 */
function _doublefield_formatter_field_formatter_settings_form($field, $display, &$form, &$form_state) {
	$settings = (isset($display['settings']) && is_array($display['settings']))? $display['settings'] : array();
	$element = array();
	switch ($display['type']) {
		case 'doublefield_formatter':
			
			$element['wrapper_tag'] = array(
					'#title' => t('Wrapper tag'),
					'#description' => t('Leave blank to to remove wrapper altogether'),
					'#type' => 'textfield',
					'#default_value' => $settings['wrapper_tag'],
					'#element_validate' => array('doublefield_formatter_validate_tag'),
					'#size' => 9
			);
			
			$element['wrapper_classes'] = array(
					'#title' => t('Wrapper classes'),
					'#type' => 'textfield',
					'#default_value' => $settings['wrapper_classes'],
					'#element_validate' => array('doublefield_formatter_validate_classes'),
					'#size' => 32
			);
			
			$var_name = 'suppress_if_empty';
			$element[$var_name] = array(
					'#title' => t("Suppres subfield if empty"),
					'#type' => 'checkbox',
					'#description' => t("If enabled, empty subfields will render no HTML, but if present wrappers will have an extra class name"),
			);
			if (!isset($settings[$var_name])) {
				$settings[$var_name] = 0;
			}
			else {
				$settings[$var_name] = (int) $settings[$var_name];
			}
			if ($settings[$var_name] == 1) {
				$element[$var_name]['#attributes'] = array('checked' => TRUE);
			}
			$item_settings = $field['settings']['first'];
			_doublefield_formatter_item_settings($element,$settings,'first', $item_settings);
			
			$item_settings = $field['settings']['second'];
			_doublefield_formatter_item_settings($element,$settings,'second',$item_settings);
			
			break;
	}
	return $element;
}

/*
 * Settings for item component (first or second
 * @param array $element
 * @param array $settings
 * @param string $type 
 * @param array $item_settings
 * @return void
 */
function _doublefield_formatter_item_settings(&$element, &$settings, $type='first', $item_settings = array()) {
	
	$element[$type] = array(
			'#type' => 'fieldset',
			'#title' => ucwords($type) . ' sub-field ',
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
			'#attributes' => array('class' => array('doublefield-item-settings'))
	);
	
	$var_name = 'tag';
	$element[$type][$var_name] = array(
			'#title' => t(ucwords($type) . ' item tag'),
			'#type' => 'textfield',
			'#default_value' => $settings[$type][$var_name],
			'#required' => TRUE,
			'#element_validate' => array('doublefield_formatter_validate_tag'),
			'#size' => 9
	);
	
	$var_name = 'prefix';
	$element[$type][$var_name] = array(
			'#title' => t(ucwords($type) . ' item prefix'),
			'#type' => 'textfield',
			'#default_value' => $settings[$type][$var_name],
			'#size' => 4
	);

	$var_name = 'suffix';
	$element[$type][$var_name] = array(
			'#title' => t( ucwords($type) . ' item suffix'),
			'#type' => 'textfield',
			'#default_value' => $settings[$type][$var_name],
			'#size' => 4
	);
	
	$var_name = 'classes';
	$element[$type][$var_name] = array(
			'#title' => t( ucwords($type) . ' item classes'),
			'#type' => 'textfield',
			'#default_value' => $settings[$type][$var_name],
			'#element_validate' => array('doublefield_formatter_validate_classes'),
			'#size' => 32
	);
	
	$var_name = 'format';
	if ($item_settings['type'] == 'varchar' && $item_settings['maxlength'] > 16) {
		$element[$type][$var_name] = array(
				'#title' => t( ucwords($type) . ' text format'),
				'#type' => 'select',
				'#options' => _doublefield_formatter_text_formats(),
				'#default_value' => $settings[$type][$var_name],
		);
	}
	else {
		$element[$type][$var_name] = array(
				'#type' => 'hidden',
				'#value' => 'plain_text'
		);
	}
	
}

/*
 * Fetch associative array of text formats
 * @return array
 */
function _doublefield_formatter_text_formats() {
	$options = array('plain_text' => 'Plain text');
	foreach (filter_formats() as $format) {
			$options[$format->format] = $format->name;
	}
	return $options;
}

/*
* Called from  doublefield_formatter_field_formatter_settings_summary() in module file
* @param array $settings
* @param array $instance
* @param string $view_mode
* @param string $display_type
* @return string
*/
function _doublefield_formatter_field_formatter_settings_summary($field, $instance, $view_mode,$display_type = NULL) {
	$summary = '';
	
	$display = isset($instance['display'][$view_mode])? $instance['display'][$view_mode] : array();
	$settings = isset($display['settings'])? $display['settings'] : array();
	
	$summary .= "<br />" . _doublefield_formatter_item_summary($settings,'wrapper');
	
	$value = (isset($settings['suppress_if_empty']) && $settings['suppress_if_empty'] == 1)? 'yes' : 'no'; 
	
	$summary .= "<br />" .t("Suppress subfield if empty") .": " . $value;
	
	$summary .= "<br />" . _doublefield_formatter_item_summary($settings,'first');
	
	$summary .= "<br />" . _doublefield_formatter_item_summary($settings,'second');
	
	return $summary;
}

/*
 * Called from _doublefield_formatter_field_formatter_settings_summary() for each formattable tag
 * @param array $settings
 * @param string $type
 */
function _doublefield_formatter_item_summary($settings, $type = 'first') {
	$skipAttrs = false;
	if ($type == 'wrapper') {
		$name_prefix = 'wrapper_';
		$var_name = $name_prefix. 'tag';
		if (empty($settings[$var_name])) {
			$settings[$var_name] = '[none]';
			$skipAttrs = true;
		}
		$itemType = '';
	}
	else {
		$var_name = 'tag';
		$itemType = ' ' . t("subfield");
		$name_prefix = '';
	}
	$html = ucwords($type) . $itemType . " " .t("formatting").":<br />";
	if ($type == 'wrapper') {
		$value = $settings[$var_name];
	}
	else {
		$value = $settings[$type][$var_name];
	}
	$attrs = array(t("Tag:"). " ". $value);
	if (!$skipAttrs) {
		$var_name = $name_prefix. 'classes';
		if (!empty($settings[$var_name])) {
			if ($type == 'wrapper') {
				$value = $settings[$var_name];
			}
			else {
				$value = $settings[$type][$var_name];
			}
			$attrs[] = t("classes:"). " ". $value;
		}
		
		if ($type != 'wrapper') {
			$var_name = 'prefix';
			if (!empty($settings[$var_name])) {
				$attrs[] = t("prefix:"). " ". $settings[$var_name];
			}
			$var_name = 'suffix';
			if (!empty($settings[$var_name])) {
				$attrs[] = t("suffix:"). " ". $settings[$type][$var_name];
			}
			
			$var_name = 'format';
			if (isset($settings[$var_name]) && $settings[$type][$var_name] != 'plain_text') {
				$attrs[] = t("classes:"). " ". $settings[$type][$var_name];
			}
		}
	}
	
	$html .= implode(', ', $attrs);
	
	return $html;
	
}
